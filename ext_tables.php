<?php
if (!defined('TYPO3_MODE')) {
    die ('Access denied.');
}

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile('gridconfigurator', 'Configuration/TypoScript', 'Gridconfigurator');
\FluidTYPO3\Flux\Core::registerProviderExtensionKey('gridconfigurator', 'Content');
