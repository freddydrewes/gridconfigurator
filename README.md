# Gridkonfigurator
Der Gridkonfigurator dient dazu die Webseite via Bootstrap Grids zu organisieren und verwalten. 
Dadurch ist es möglich zum Beispiel zwei oder dreispaltige Elemente auf der Webseite anzulegen.

### Installation
Der Gridkonfigurator kann entweder über das TER installiert oder aber via zip im TYPO3 direkt installiert werden. 
Ebenso möglich ist das kopieren des Git Respositorys direkt in das "ext" Verzeichnis unter typo3conf.


### Hinweis:
Es kann pro Zeile (.row) eine Hintergrundfarbe gewählt werden. Stehen die Farben vorher fest
so können diese via der LocalConfiguration.php als array angelegt werden und ausgelesen werden. 
Ist kein array in der LocalConfiguration.php angegegeben, erhält man einen freien Colorpicker.

### Angabe in der LocalConfiguration.php

```
    'EXTCONF' => [
        'gridconfigurator' => [
            'colors' => [
                ['schwarz', '#000'],
                ['weiß', '#fff'],
                [...weitere Farben als array...],
            ]
        ]
    ],
```


