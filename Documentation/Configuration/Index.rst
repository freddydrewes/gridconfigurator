.. include:: ../Includes.txt


.. _configuration:

=============
Configuration
=============

Target group: **Developers, Integrators**

If you want to add restricted colours to the gridelement backend settings you have to set an color array inside of :file:`LocalConfiguration.php`.

.. code-block:: php

    'EXTCONF' => [
        'gridconfigurator' => [
            'colors' => [
                ['black', '#000'],
                ['white', '#fff'],
            ]
        ]
    ],

.. _configuration-php:

.. figure:: ../Images/Backend/grid-colour-settings.png
   :class: with-shadow
   :alt: colour settings

If the color array has not been set, then you have a Colorpicker to choose a color.

.. figure:: ../Images/Backend/grid-colorpicker.png
   :class: with-shadow
   :alt: colour picker