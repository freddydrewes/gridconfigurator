.. include:: ../Includes.txt


.. _for-editors:

===========
For Editors
===========

Target group: **Editors**

How to use the extension from the perspective of an editor.

After installation and configuration it is easy to use the extension.

Choose a site within the site modul, where you want to put a grid.

If you want to place a gridelement on the frontpage for example, you would choose "Startseite" or "frontpage/home".

Click on the "+ content" button to add any kind of content.

There are now a new Tab called "Gridkonfigurator" where you can choose the Element "Gridkonfigurator".

.. figure:: ../Images/Backend/gridconfigurator.PNG
   :class: with-shadow
   :alt: add content element

After that your getting a new mask. In the first step you are able to choose which amount of coloumns you want to display.
You can choose from 1 to 12 and additionally 1/3 & 2/3 and 1/4 & 3/4.

.. figure:: ../Images/Backend/coloumn-amount.PNG
   :class: with-shadow
   :alt: add content element

In the settings tab are more settings opportunities.

* Width of the row element
* If the row does have a backgroundimage
* If the row should have a Parallax Effect
* Background Color of the row.
* Padding and Marin of the row.