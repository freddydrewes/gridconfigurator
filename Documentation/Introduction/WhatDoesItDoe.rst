.. include:: ../Includes.txt


.. _introduction:

============
What does it do?
============

This extension implements a grid system based on the latest technologies which are provided by the TYPO3 CMS.

It is based on extbase, fluid and flux.

It's possible to structure the content via the `bootstrap framework? <https://getbootstrap.com/>`__.
Each row can be customized with a different background-color and more options.

Read more at page `For Editors <Editor/Index.html>`__.
