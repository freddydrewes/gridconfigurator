.. include:: ../Includes.txt


.. _introduction:

============
Introduction
============

The aim of this chapter is to provide a general overview of your extension.

* `What does it do? <WhatDoesItDoe.html>`__
* `What problems does it solve? <WhatDoesItSolve.html>`__
* `Need help? <NeedHelp.html>`__
