.. include:: ../Includes.txt


.. _introduction:

============
What problem does it solve?
============

The gridconfigurator displays several HTML Elements side by side, without the need of coding one line.

See the following figure as example:

Backend view
================

.. figure:: ../Images/Backend/backend.PNG
   :class: with-shadow
   :alt: Backend view

Frontend View
================

.. figure:: ../Images/Frontend/frontend.PNG
   :class: with-shadow
   :alt: Frontend view