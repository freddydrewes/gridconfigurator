.. include:: ../Includes.txt



.. _installation:

============
Installation via Extension Manager
============

Target group: **Administrators**

To install the extension via the TYPO3 Extension Manager you have to log in as the Administrator User.
Navigate to "Extensions" and switch to "Get Extensions":

.. figure:: ../Images/Backend/extensionmanager.png
   :class: with-shadow
   :alt: Extensionmanager view

Enter "gridconfigurator" in the Searchform and press "Enter".
After that activate the extension by clicking the toggle left to the Extensionname.
