.. include:: ../Includes.txt



.. _installation:

============
Installation
============

Target group: **Administrators**

You can install the extension via the TYPO3 extension manager.

* `Extension Manager <ExtensionManager.html>`__
* `First steps <Integration.html>`__



