.. include:: ../Includes.txt



.. _installation:

============
First steps
============

Target group: **Administrators**

After activating the Extension it is necessary to include it in the page template.

To do that edit the page template via "Template".

.. figure:: ../Images/Backend/modifysetup.png
   :class: with-shadow
   :alt: modify templates

Click on the button "Edit the whole template record". Navigate to the tab "Includes" and click on "Gridconfigurator" in the "Available Items" column and save the settings.

.. figure:: ../Images/Backend/availableitems.png
   :class: with-shadow
   :alt: modify templates

