<?php
/**
 * Created by PhpStorm.
 * User: Frederik Drewes
 * Date: 22.08.2019
 * Time: 14:55
 */
namespace Configurator\Gridconfigurator\ViewHelpers;

use TYPO3\CMS\Fluid\Core\Rendering\RenderingContextInterface;
use TYPO3\CMS\Extbase\Utility\DebuggerUtility;
use TYPO3\CMS\Core\Utility\GeneralUtility;

class TestViewHelper extends \TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper
{
    /**
     * Children must not be escaped, to be able to pass {bodytext} directly to it
     *
     * @var bool
     */
    protected $escapeChildren = false;

    /**
     * @var bool
     */
    protected $escapeOutput = false;

    /**
     * Initialize arguments.
     *
     * @throws \TYPO3Fluid\Fluid\Core\ViewHelper\Exception
     */
    public function initializeArguments() {
        parent::initializeArguments();
    }

    /**
     * ViewHelper um die Farbe aus den Seiteneigenschaften zu erhalten.
     *
     * @return array
     */
    public function render() {
        return $GLOBALS['TYPO3_CONF_VARS']['EXTCONF']['gridconfigurator']['colors'];
    }

}