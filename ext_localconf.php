<?php

/**
 * Created by PhpStorm.
 * User: Frederik Drewes
 * Date: 16.05.2018
 * Time: 19:19
 */
use TYPO3\CMS\Extbase\Utility\DebuggerUtility;

if (!defined('TYPO3_MODE')) {
    die ('Access denied.');
}




\FluidTYPO3\Flux\Core::registerProviderExtensionKey('Fd.Gridconfigurator', 'Content');
