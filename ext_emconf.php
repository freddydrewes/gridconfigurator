<?php

/***************************************************************
 * Extension Manager/Repository config file for ext "myextensionkey".
 *
 * Auto generated 31-10-2018 19:55
 *
 * Manual updates:
 * Only the data in the array - everything else is removed by next
 * writing. "version" and "dependencies" must not be touched!
 ***************************************************************/

$EM_CONF[$_EXTKEY] = array(
    'title' => 'Gridkonfigurator',
    'description' => 'Der Gridkonfigurator dient dazu die Webseite via Bootstrap Grids zu organisieren und verwalten. Dadurch ist es möglich zum Beispiel zwei oder dreispaltige Elemente auf der Webseite anzulegen.',
    'category' => 'misc',
    'shy' => 0,
    'version' => '0.0.5',
    'dependencies' => 'cms,extbase,flux,vhs',
    'conflicts' => '',
    'priority' => '',
    'loadOrder' => '',
    'module' => '',
    'state' => 'beta',
    'uploadfolder' => 0,
    'createDirs' => '',
    'modify_tables' => '',
    'clearcacheonload' => 1,
    'lockType' => '',
    'author' => 'Frederik Drewes',
    'author_email' => 'info@frederik-drewes.de',
    'author_company' => '',
    'CGLcompliance' => '',
    'CGLcompliance_note' => '',
    'constraints' => array(
        'depends' => array(
            'typo3' => '8.0.0-9.9.99',
            'cms' => '',
            'extbase' => '',
            'flux' => '',
            'vhs' => '',
        ),
        'conflicts' => array(
        ),
        'suggests' => array(
        ),
    ),
    '_md5_values_when_last_written' => 'a:0:{}',
    'suggests' => array(
    ),
);
